  # simulate data
  let S := N; 
  let{i in 1..S, m in 1..M} y[i,m] := exp(Normal(2,1));
  let{i in 1..N, m in 1..M} yo[i,m] := y[i,m];
  let{j in 1..J,m in 1..M,i in 1..S} eps[j,m,i] := 
      -log(-log(Uniform(0,1)));
  let{j in 1..J,m in 1..M} xi[j,m] := Normal(0,1);
  display 1/(J*M*S)*sum{j in 1..J,m in 1..M,i in 1..S} eps[j,m,i]; 
  # instruments = 
  # (constant, sum other xi, things correlated with x's but not xi)
  let{j in 1..J,m in 1..M} z[j,m,1] := 1; # include constant as instrument
  let{j in 1..J,m in 1..M} z[j,m,2] := 1/J*sum{j2 in 1..J} 
          if (j2==j) then 0 else xi[j2,m];  

  let {j in 1..J,m in 1..M,k in 1..Kp} x[j,m,k] := Normal(0,1);
  let {j in 1..J,m in 1..M,k in 3..Kz} z[j,m,k] := x[j,m,1]^(k-2);

  #let {j in 1..J} x[j,1] := 1;
  #let {j in 1..J,k in 1..Kp} x[j,k] := z[j,m,k+2];   
               #1/sqrt(M)*(sum{m in 1..M} xi[j,m]) + 
               #  + (sum{kz in 3..Kz,m in 1..M} z[j,m,kz])/sqrt((Kz-2)*M) 
               #  + Normal(0,1);
  let{j in 1..J,m in 1..M} delta[j,m] := xi[j,m] + sum{k in 1..Kp} x[j,m,k]*beta[k];
  let{m in 1..M,k in 1..Ki} dm[m,k] := Normal(0,1);
  let{i in 1..S, m in 1..M} dim[i,m] := Normal(0,1);
  let{i in 1..S,m in 1..M,k in 1..Ki} d[i,m,k] := dm[m,k] + dim[i,m] + Normal(0,1);
  let{i in 1..N,m in 1..M,k in 1..Ki} w[i,m,k] := d[i,m,k];
  let{i in 1..S,m in 1..M,k in 1..Kp} nu[i,m,k] := Normal(0,1);

  # find prices  
  let{j in 1..J,m in 1..M} c[j,m,1] := 1;
  let{j in 1..J,m in 1..M,k in 2..Kc} c[j,m,k] := Normal(0,0.1);
  let{j in 1..J,m in 1..M,k in 1..Kc} zc[j,m,k] := c[j,m,k];
  let{j in 1..J,m in 1..M,k in 1..Kc} zc[j,m,k+Kc] := c[j,m,k]^2;
  let{j in 1..J,m in 1..M} zc[j,m,2*Kc+1] := 1;
  let{j in 1..J,m in 1..M} etaSim[j,m] := Normal(0,0.1);
  let{j in 1..J,m in 1..M} p[j,m] := exp((sum{k in 1..Kc} alphac[k]*c[j,m,k])+etaSim[j,m]); 
  drop moments; drop shares;
  fix alpha; fix beta; fix alphac; fix delta; fix pi; fix sig;
  unfix p; restore priceCon;
  option solver snopt;
  option snopt_options 'iterations=10000 outlev 1 timing 1';
  solve;
 
  # simulate choices
  for{m in 1..M,i in 1..S} {
    let maxU := alpha*log(y[i,m])-log(-log(Uniform(0,1))); # outside option
    let choice[m,i] := 0;
    for{j in 1..J} {
      if (mu[j,m,i]+eps[j,m,i] >= maxU) then {
         let maxU := mu[j,m,i]+eps[j,m,i];
         let choice[m,i] := j;
      }
    }
  }
  #let{j in 1..J,m in 1..M} s[j,m] := 1/S * sum{i in 1..S} 
  #  (if (choice[m,i]=j) then 1 else 0);
  
  # lower simulation error by setting s = E[s]
  let{j in 1..J,m in 1..M} s[j,m] := 1/S * sum{i in 1..S} pbuy[i,j,m];

  # done simulating, let's print some summary stats
  display {j in 1..J} 1/M*(sum{m in 1..M} s[j,m]);
  display {j in 1..J} 1/M*(sum{m in 1..M} p[j,m]);
  # check that instruments are correlated with prices
  display {k in 2..Kz,j in 1..J} 
     (sum{m in 1..M} (p[j,m]-(sum{m1 in 1..M} p[j,m1])/M)*
                      (z[j,m,k]-(sum{m1 in 1..M} z[j,m1,k])/M))
     / sqrt((sum{m in 1..M} (p[j,m]-(sum{m1 in 1..M} p[j,m1])/M)^2)*
            (sum{m in 1..M} (z[j,m,k]-(sum{m1 in 1..M}  z[j,m1,k])/M)^2)+1e-300);
  # check that instruments are uncorrelated with xi
  display {k in 2..Kz,j in 1..J} 
     (sum{m in 1..M} (xi[j,m]-(sum{m1 in 1..M} xi[j,m1])/M)*
                      (z[j,m,k]-(sum{m1 in 1..M} z[j,m1,k])/M))
     / sqrt((sum{m in 1..M} (xi[j,m]-(sum{m1 in 1..M} xi[j,m1])/M)^2)*
            (sum{m in 1..M} (z[j,m,k]-(sum{m1 in 1..M} z[j,m1,k])/M)^2)+1e-300);

#  display p,eta,etaSim,omega;
#  display {j in 1..J} sum{k in 1..Kp} x[j,m,k]*beta[k];
#  display {j in 1..J} 1/M*(sum{m in 1..M} delta[j,m]);
#  display {j in 1..J} 1/(M*S)*sum{m in 1..M,i in 1..S} mu[j,m,i];
#  display{j in 1..J} 1/(M*S)*sum{m in 1..M,i in 1..S} 
#    exp(mu[j,m,i]) / (1 + sum{j2 in 1..J} exp(mu[j2,m,i]));
