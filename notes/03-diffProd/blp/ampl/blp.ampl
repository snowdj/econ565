# set up data
model blpSupply.mod; # tells ampl which .mod file to use

# set size of data
let Kp := 1; # number of product characteristics 
let Ki := 1; # number of individual characteristics
let J := 2; # number of products
let N := 500; # number of individual observations
let M := 75; # number of markets / time periods
let Kz := 4; # number of instruments
let Kc := 1; # number of cost shifters (first one is a constant)
let Kzc := 2*Kc+1; # number of cost instruments

option randseed 270; 

# write header in output file
printf 'price_solve_num,solve_result_num (0=success),' >> blp.sigBdd.csv;
printf 'alpha,' >> blp.sigBdd.csv;
printf{k in 1..Kp} 'beta %d,', k >> blp.sigBdd.csv;
printf{k in 1..Kp} 'sig %d,', k >> blp.sigBdd.csv;
printf{k in 1..Kp,ki in 1..Ki} 'pi %d %d,', k,ki >> blp.sigBdd.csv;
printf{k in 1..Kc} 'alphac %d,', k >> blp.sigBdd.csv;
printf{j in 1..J,m in 1..M} 'delta %d %d,',j,m >> blp.sigBdd.csv;
printf '\n' >> blp.sigBdd.csv;

# variables used in simulation
param dim{i in 1..S,m in 1..M};
param dm{i in 1..S,m in 1..M};
param xi{j in 1..J,m in 1..M}; 
param eps{j in 1..J,m in 1..M,i in 1..S}; 
param choice{1..M,1..S};
param maxU;
param pSolve;

for{mc in 1..100} { # do many repetitions
  # reset parameters
  let{k in 1..Kp} beta[k] := 1;
  let{k in 1..Kp, ki in 1..Ki} pi[k,ki] := 1;
  let{k in 1..Kp} sig[k] := 1;
  let{k in 1..Kc} alphac[k] := 1;
  let alpha := 1;

  let{kz1 in 1..Kz,kz2 in 1..Kz,m1 in 1..M,m2 in 1..M}
    W[kz1,kz2,m1,m2] := if (kz1=kz2 and m1=m2) then 1 else 0;
  # if don't want to use supply moments, set Wc to 0.
  let{kz1 in 1..Kzc,kz2 in 1..Kzc,m1 in 1..M,m2 in 1..M}
    Wc[kz1,kz2,m1,m2] := 0; # if (kz1=kz2 and m1=m2) then 1 else 0;
  let{j in 1..J,m in 1..M} s[j,m] := 1/J;

  include simulateData.ampl;
  let pSolve := solve_result_num;

  # estimation section
  let S := N;
  # redraw nu and d to be fair
  let{i in 1..S,m in 1..M,k in 1..Ki} d[i,m,k] := w[ceil(Uniform(0,N)),m,k];
  let{i in 1..S,m in 1..M} y[i,m] := yo[ceil(Uniform(0,N)),m];
  let{i in 1..S,m in 1..M,k in 1..Kp} nu[i,m,k] := Normal(0,1);
  
  # choose solver and set options
  let alpha := alpha/2;
  option solver snopt;
  option snopt_options 'iterations=10000 outlev 1 timing 1';

  fix p; drop priceCon; drop niceP;
  restore moments; restore shares; 
  unfix alpha; unfix beta; unfix delta; unfix pi; unfix sig; unfix alphac;  
  solve;
  #display results
#  display delta;
#  display {j in 1..J} 1/(M*S)*sum{m in 1..M,i in 1..S} mu[j,m,i];
#  display{j in 1..J} 1/(M*S)*sum{m in 1..M,i in 1..S} 
#    exp(mu[j,m,i]) / (1 + sum{j2 in 1..J} exp(mu[j2,m,i]));
#  display{j in 1..J} 1/M*sum{m in 1..M} s[j,m];
  display beta,sig,pi;
  display alpha;
  #display {j in 1..J} 1/M*(sum{m in 1..M} eta[j,m]);
  #display p,eta,etaSim,omega;

  # print results
  printf '%d,%d,', pSolve,solve_result_num >> blp.sigBdd.csv;         
  printf '%.8g,', alpha >> blp.sigBdd.csv;
  printf{k in 1..Kp} '%.8g,', beta[k] >> blp.sigBdd.csv; 
  printf{k in 1..Kp} '%.8g,', sig[k] >> blp.sigBdd.csv;
  printf{k in 1..Kp,ki in 1..Ki} '%.8g,', pi[k,ki] >> blp.sigBdd.csv;
  printf{k in 1..Kc} '%.8g,', alphac[k] >> blp.sigBdd.csv;
  printf{j in 1..J,m in 1..M} '%.8g,', delta[j,m] >> blp.sigBdd.csv;
  printf '\n' >> blp.sigBdd.csv;
  display mc;
  printf '###################################################\n'
}




