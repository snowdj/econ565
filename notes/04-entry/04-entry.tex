\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}
\graphicspath{{snapshots/}}

\title{Market entry}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\part{Overview of market entry}

\frame{\partpage}

\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{Aguirregabiria2012} chapter 5
    \item \cite{sutton1991}
    \item \cite{levin2009}
    \end{itemize}
  \item Key papers:
    \begin{itemize}
    \item \cite{br1991}
    \end{itemize}
  \end{itemize} 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame}[allowframebreaks]\frametitle{Introduction}
  \begin{itemize}
  \item Models of entry:
    \begin{itemize}
    \item Dependent variable $=$ firm decision to operate or not in a
      market
      \begin{itemize}
      \item Enter industry, open new store, introduce new product,
        release a new movie, bid in an auction
      \end{itemize}
    \item Sunk cost from being active in market
    \item Payoff of being active depends on how many other firms are
      in the market (game)
    \end{itemize}
    \[ a_{im} = 1\left\lbrace
      \Pi_{im}(N_m,X_{im},\epsilon_{im}) \geq 0 \right\rbrace \]
  \item Estimate $\Pi$ using revealed preference
  \item Static models: entry $\approx$ being in active in market; not
    transition in/out 
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Why estimate models of entry?}
  \begin{itemize}
  \item Why not just estimate payoff function using demand and production
    estimation techniques?
  \item \alert{Efficiency}: entry conditions provide additional
    information about payoffs, so using them can give us more precise
    estimates
  \item \alert{Identification}: some parameters (e.g.\ fixed costs)
    can only be identified from entry
  \item \alert{Requires less data}: price and quantity data not needed
    for some entry models
  \item \alert{Controlling for selection}
  \end{itemize}
\end{frame}

\section{\cite{br1991}}

\begin{frame} \frametitle{\cite{br1991}}
  \begin{itemize}
  \item Can learn a lot from market entry with very limited data 
  \item Cross-section of isolated markets where we observe
    \begin{itemize}
    \item Number of firms
    \item Some market characteristics (prices and quantities not
      needed) 
    \end{itemize}
  \item Identify:
    \begin{itemize}
    \item Fixed costs
    \item Degree of competition: payoffs $=f(\text{number of firms})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setting}
  \begin{itemize}
  \item Questions:
    \begin{itemize}
    \item Degree of competition: how fast profits decline with $n_m$
    \item How many entrants needed to achieve competitive equilibrium
      (contestable markets)
    \end{itemize}
  \item Data:
    \begin{itemize}
    \item Retail and professional industries (doctors, dentists,
      pharmacies, car dealers, etc.), treat each industry separately
    \item $M$ markets 
    \item $n_m$ firms per market
    \item $S_m$ market size
    \item $x_m$ market characteristics
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model}
  \begin{itemize}
  \item $N$ potential entrants
  \item Profit of each firm when $n$ active $= \Pi_m(n)$
    \begin{itemize}
    \item $\Pi_m$ decreasing in $n$
    \end{itemize}
  \item Equilibrium: 
    \[ \Pi_m(n_m) \geq 0 \;\text{ and }\; P_m(n_m+1) < 0 \]
  \item Profit function:
    \begin{align*} 
      \Pi_m(n) = & \underbrace{V_m(n)}_{\text{variable}} -
      \underbrace{F_m(n)}_{\text{fixed}}  \\
      = & S_m v_m(n) - F_m(n) \\
      = & S_m \left(x_m^D \beta - \alpha(n) \right) -
      \left(x_m^c\gamma + \delta(n) + \epsilon_m \right) 
    \end{align*}
    where
    \begin{itemize}
    \item $\alpha(1) \leq \alpha(2) \leq \cdots \leq \alpha(N)$
    \item $\delta(1) \leq \delta(2) \leq \cdots \leq \delta(N)$
      \begin{itemize}
      \item Entry deterrence, firm heterogeneity, real estate prices
      \end{itemize}
    \item Key difference between variable and fixed profits is that
      variable depend on $S_m$, fixed do not
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Parameters $\theta = (\beta,\gamma,\alpha,\delta)$
  \item MLE
    \[ \hat{\theta} = \argmax_\theta \sum_{m=1}^M \log \Pr(n_m | x_m,
    S_m; \theta) \]
  \item Assume $\epsilon_m \sim N(0,1)$, independent of $x_m,S_m$ 
    {\footnotesize{
    \begin{align*}
      \Pr(n | x_m, S_m; \theta) 
      = & \Pr\left(\Pi_m(n) \geq 0 >
        \Pi_m(n+1) \right) \\
      = & \Pr\begin{pmatrix} S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \geq \epsilon \\ \epsilon > S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)
      \end{pmatrix}    \\
      =
      & \includegraphics[width=0.3\textwidth,height=0.12\textwidth]{br-illustration} \\
      = & \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \right) - 
      \\ & - \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)\right)
    \end{align*}
  }}
  \end{itemize}
\end{frame}
  
\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item 202 isolated local markets 
    \begin{itemize}
    \item Population 500-75,000
    \item $\geq$ 20 miles from nearest town of 1,000$+$
    \item $\geq$ 100 miles from city of 100,000$+$
    \end{itemize}
  \item 16 industries: retail and professions, each estimated
    separately
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item For most industries, $\alpha(n)$ and $\delta(n)$ increase with
    $n$
  \item Define $S(n)=$ minimal $S$ such that $n$ firms enter
    \[ S(n) = \frac{x_m^C\gamma + \delta(n)}{x_m^D\beta - \alpha(n)} \]
    \begin{itemize}
    \item Varies across industries
    \end{itemize}
  \item $\frac{S(n)}{n} \approx$ constant for $n \geq 5$ 
    \begin{itemize}
    \item Contestable markets \citep{baumol1982} : an industry can be
      competitive even with few firms if there is easy entry
    \end{itemize}
  \end{itemize}  
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{br-thresholdTable}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{br-thresholdFigure}
\end{frame}

\section{Other applications}
\begin{frame}\frametitle{Other applications}
  \begin{itemize}    
  \item Supermarkets: 
    \begin{itemize}
    \item \cite{bronnenberg2009}
    \item \cite{jia2008happens}
    \item \cite{ellickson2007}
    \end{itemize}
  \item Airlines:
    \begin{itemize}
    \item \cite{berry1992}
    \item \cite{ciliberto2009}
    \end{itemize}
  \item Radio: \cite{sweeting2009}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\part{Entry in supermarkets and retail}
\frame{\partpage}
\begin{frame}
  \tableofcontents  
\end{frame}

\section{\cite{bronnenberg2009}}

\begin{frame}\frametitle{\cite{bronnenberg2009}}
  \begin{itemize}
  \item Style of paper: document interesting pattern in data that has
    not been highlighted before
  \item Looks at market shares of brands of consumer packaged goods (CPG) across
    markets and time 
    \begin{itemize}
    \item CPG $=$ beer, coffee, ketchup, etc.
    \end{itemize}
  \item Results
    \begin{itemize}
    \item Market shares variable across geographic markets, but
      persistent over time within each market
    \item Market shares spatially correlated
    \item Spatial market shares strongly correlated with first mover
      advantage 
      \begin{itemize}
      \item e.g.\ Miller (founded in Milwaukee) most popular beer in
        Milwaukee, Budweiser (founded in St. Louis) most popular beer
        in St.\ Louis 
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Market shares from AC Nielsen scanner data
    \begin{itemize}
    \item This type of data has been used very frequently in IO during
      the last decade
    \item AC Nielsen distributes bar code scanners to a sample of
      consumers, consumers record every purchase by scanning bar codes
    \item 4-week intervals, June 1992-May 1995
    \end{itemize}
  \item $\mathrm{Share}_{icmt} = \frac{\mathrm{Sales}_{icmt}}{\sum_i
      \mathrm{Sales}_{icmt}}$
  \item $\mathrm{Share}_{icm} = \frac{\mathrm{Sales}_{icmt}}{\sum_t \sum_i
      \mathrm{Sales}_{icmt}}$
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd1}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd2}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd3}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd4}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd5}
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{bdd6}
\end{frame}

\begin{frame}\frametitle{Conclusions}
  \begin{itemize}
  \item Possible explanations:
    \begin{itemize}
    \item Endogenous sunk costs \citep{sutton1991}: early entrant
      invests in advertising (or something else that increases
      vertical quality), which creates high fixed cost of subsequent
      entry 
    \item Brand preference inertia
    \end{itemize}
  \item Future research:
    \begin{itemize}
    \item When can persistence be broken? 0
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{ellickson2007}}

\begin{frame}[allowframebreaks]\frametitle{\cite{ellickson2007}}
  \begin{itemize}
  \item Style of paper: (1) theoretic model with stylized predictions
    (2) empirical evidence supporting stylized predictions
  \item Model: endogenous fixed costs \citep{sutton1991} adapted to
    supermarkets 
    \begin{itemize}
    \item Vertical quality $=$ variety of products
    \item Firms with low vertical quality cannot survive
    \item As market grows, existing firms increase quality, which
      requires larger stores and more sophisticated distribution
      (fixed costs)
    \item Non fragmentation: Higher fixed costs in larger markets
      means number of firms does not increase with market size  
    \item Fragmentation: if fixed costs were constant more firms would
      enter larger markets and market share of each firm would decline
    \end{itemize}
  \item Empirical results:
    \begin{itemize}
    \item 4-6 supermarkets capture most market share regardless of
      market size
    \item Industry without fixed costs related to vertical quality
      (barber shops and beauty salons) have shares of each
      firm declining with market size
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Consumer utility:
    \[ u(\underbrace{x_1}_{\text{other
        goods}},\underbrace{x_2}_{\text{groceries}},\underbrace{z}_{\text{quality}})
    = (1-\alpha) \log(x_1) + \alpha \log(z x_2) \] 
  \item Supermarket costs function: 
    \[ C(p_L,w,p_g;q_j,z_j) = p_L \sigma
    + \frac{\lambda p_L}{\gamma} (z_j^\gamma - 1) 
    + \underbrace{c}_{=\phi_1 w + \phi_2 p_g +\phi_3 p_L} q_j \]
    quantity $q_j$, quality $z_j$, prices $p_L$ (land), $w$ (labor),
    $p_g$ (inputs), and parameters $\sigma$, $\lambda$, $\gamma$
  \item Shephard's lemma
    \begin{align*}
      h_L(p_L,w,p_g;q_j,z_j) = & \frac{\partial C}{\partial p_L} \\
      = & \sigma + \frac{\lambda}{\gamma} (z_j^\gamma - 1) + \phi_3
      q_j
    \end{align*}
  \item Equilibrium: simultaneous move, symmetric information
    \begin{enumerate}
    \item\label{e1} Choose to enter at cost $p_L \sigma$
    \item\label{e2} Choose quality at cost $\frac{\lambda p_L}{\gamma}
      (z_j^\gamma - 1)$
    \item\label{e3} Choose $q_j$ in Cournot competition
    \end{enumerate}
  \item Solving backward
    \begin{itemize}
    \item[\ref{e3}] $q = \frac{N-1}{N^2} \frac{S}{c}$ and $p(z) =
      \frac{N}{N-1} c$ 
      \begin{itemize}
      \item $N=$ number of firms, $S=$ market size
      \end{itemize}
    \item[\ref{e2}] $z = \left(\frac{2S(N-1)^2}{N^3 \lambda p_L}
      \right)^{1/\gamma}$ 
    \item[\ref{e1}] $\left(\frac{p_L(\lambda -\gamma\sigma)}{S}\right) N^3 =
      2N^2 - (4+\gamma) N + 2$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium $N$}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{e1}
\end{frame}

\begin{frame}[shrink] \frametitle{Equilibrium $N$ comparative statics}
  \begin{enumerate}
  \item ``If $\lambda - \gamma \sigma< 0$, then the left-hand side has a concave graph
    and lies below the horizontal
    axis. As shown on the left side of Figure 1, the equilibrium number of
    firms $N^\ast$ lies in the interval $(0, N^+ )$. Because the slope
    of the left-hand side decreases (in absolute value)
    as $S$ increases, the equilibrium number of firms increases as market
    size increases. This
    effect can be offset to a greater or lesser extent by an increase in
    the price of land as
    market size expands.''
  \item ``If $\lambda - \gamma \sigma> 0$, then the left-hand side has a convex graph
    and lies above the horizontal axis. Because the slope of the
    left-hand side decreases as S increases, this case has the
    somewhat counterintuitive implication that the equilibrium number
    of firms will decrease as market sizes expand, an effect that will
    be reinforced if land prices also increase.''
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{Market definition}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{e2}
\end{frame}


\begin{frame}\frametitle{Concentration and market size}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{e3}
\end{frame}

\begin{frame}\frametitle{Fragmentation in barber shops and beauty salons}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{e4}
\end{frame}

\begin{frame}\frametitle{Conclusions}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{jia2008happens}}

\begin{frame}\frametitle{\cite{jia2008happens}}
  \begin{itemize}
  \item Style of paper: structural empirical IO (like most of the
    papers we have covered)
  \item Question: impact of Wal-Mart (and Kmart) on local discount
    retailers 
    \begin{itemize}
    \item Importance of economies of scale for Wal-Mart's success? 
    \end{itemize}
  \item Model:
    \begin{itemize}
    \item Flexible competition among all players and markets
      (important for question, but makes
      model difficult to solve)
    \item Scale economies within score and across regions
    \end{itemize}
  \item Results:
    \begin{itemize}
    \item Kmart declined in importance 
    \item Entry of chain store makes 50\% of other discount stores
      unprofitable
    \item Entry of Wal-Mart explains 30-50\% of decline in other
      discount stores 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Like \cite{br1991} no firm specific price or quantity data
  \item Market (county) characteristics (population, total retail sales)
  \item Presence of Wal-Mart and Kmart in each market
  \item Number of other discount stores in each market
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{jia1}  
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{enumerate}
  \item Pre-chain: small firms compete; do not expect chain entry
  \item Chain entry: Kmart \& Wal-Mart simultaneously choose store
    locations 
  \item Small firms exit (or enter) in response
  \end{enumerate}
  \begin{itemize}
  \item Complete information except for unanticipated chain entry
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Profit function}
  \begin{itemize}
  \item Pre-chain: small firm profits
    \[ \Pi^0_{s,m} = X^0_{s,m} \beta_s + \delta_{ss} \log N^0_{s,m} +
    \sqrt{1-\rho^2} \epsilon_m^0 + \rho \eta_{s,m}^0 - SC \]
  \item Chain entry: 
    \begin{itemize}
    \item Entry indicators: $D_{i,m} \in \{0,1\}$, $D_i = (D_{i,1}, ....,
      D_{i,M})$
    \item Distance between markets $Z_{ml}$, $Z_m = (Z_{m1}, ...,
      Z_{mM})$
    \item Chain profits:
      \[ \Pi_{i} = \sum_{m=1}^M D_{i,m} \begin{pmatrix} X_m \beta_i + \delta_{ij} D_{j,m}
        + \delta_{is} \log(N_{s,m} + 1) + \\ + \delta_{ii} \sum_{l
          \neq m} \frac{D_{i,l}}{Z_{ml}} + \sqrt{1-\rho^2} \epsilon_m +
        \rho \eta_{i,m} \end{pmatrix} \]
    \end{itemize}
  \item Post chain entry small firm profits:
    \[ \Pi_{s,m} = X_m \beta_i + \sum_{i=k,w} \delta_{is} D_{i,m} +
    \delta_{ss} \log(N_{s,m}) + \sqrt{1-\rho^2} \epsilon_m +
    \rho \eta_{s,m} - SC 1\{\text{new}\} \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{Solving for equilibrium}
  \begin{itemize}
  \item Profit maximization for chain:
    \[ \max_{D_1, ..., D_m \in \{0,1\}^M} \sum_{m=1}^M D_m \left(X_m
      + \delta \sum_{l \neq m} \frac{D_l}{Z_{ml}} \right) \]
  \item Discrete strategy space, so usual optimization techniques do
    not apply
  \item In general discrete optimization is NP-hard, which in practice
    means that there is no general purpose algorithm that can solve
    large problems 
  \item $2^M = 2^{2062}$ possible $D$, so cannot brute force maximize
  \item Solution approach:
    \begin{itemize}
    \item Observe: we are maximizing profits over a discrete set, we
      know a lot about this sort of problem (monotone comparitive
      statics, supermodularity, lattice theory etc)
    \item Use results from lattice theory to devise a solution
      algorithm  
    \end{itemize}
  \item Solving single firm problem
    \begin{itemize}
    \item Necessary condition for optimizer:
      \begin{align*} 
        \Pi(D_1^*, ..., D_m^*, ..., D_M^*) \geq & \Pi(D_1^*, ..., D_m,
        ..., D_M^*) \\
        \text{implies} & \\
        D_m^\ast = & 1\left\lbrace X_m + 2 \delta \sum_{l \neq m}
          \frac{D_l^\ast}{Z_{ml}} \geq 0 \right\rbrace \equiv V_m(D)
      \end{align*}
    \item Tarski's fixed point theorem: $\mathcal{D} = $set of $D$
      s.t. $D=V(D)$ is nonempty and bounded above and below
    \item Iterating $V$ starting from $(0, ..., 0)$ and $(1, ..., 1)$
      converges in at most $M$ steps to lower and upper bound of
      $\mathcal{D}$ 
    \item Can exhaustively search between bounds to find all of $\mathcal{D}$
    \end{itemize}
  \item Solving for equibria (there will generally be many):
    \begin{itemize}
    \item $X_m$ above depends on what other chain does
    \item Topkis's theorem: best response of Wal-Mart is decreasing as
      function of actions of Kmart
    \item Solve for equilibrium by:
      \begin{enumerate}
      \item[0] Set $D_w^0 = (0,...,0)$
      \item[1] Given $D_w^T$, using method above solve for maximal best
        response of Kmart to $D_w^T$, call this $D_k^T$ 
      \item[2] Given $D_k^{T-1}$ solve for minimal best response of
        Wal-Mart, call this $D_w^T$
      \item[3] Goto 1
      \end{enumerate}
      Converges to most profitable equilibrium for Kmart. Switching
      roles gives most profitable equilibrium for Wal-Mart. 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item Method of simulated moments
  \item Moments $=$ observed market structures $-$ market structure
    predicted by model (computed by simulation)
  \item Variance of estimates is complicated by spatial correlation
    \begin{itemize}
    \item Asymptotic normality requires spatial correlation to die out
      as distance increases (mixing condition)    
    \item Spatial correlation in model is endogenous (depends on
      $\delta_{ii}$)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item Look at paper for tables
  \item Parameter estimates: expected signs? magnitude for Wal-Mart vs
    Kmart?
  \item Fit: Table VI, VII
  \item Table VIII: appears to usually be a unique equilibrium
  \item Table IX-XI: comparative statics of market size and number of
    stores 
  \item Table XII: competition and chain effects
  \item Table XIII-XV: other stores with and without Wal-Mart
  \item Table XVI: subsidies and employment
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=30]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=31]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=34]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=35]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=37]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=39]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=41]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=43]{jia2008}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight,page=44]{jia2008}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}